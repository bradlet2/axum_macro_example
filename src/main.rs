use axum::{extract::Path, routing::get, Router};
use postgres_flatten::{flattened::FromFlattenedSql, FromFlattenedSql};
use std::sync::Arc;
use tokio_postgres::{Client, NoTls};

#[allow(dead_code)]
#[derive(Debug, FromFlattenedSql)]
struct Cat {
    name: String,
    age: i32,
    color: i32,
    friendliness: i32,
}

#[tokio::main]
async fn main() {
    let client = match tokio_postgres::connect(
        "host=localhost dbname=postgres port=32768 user=brad password=password",
        NoTls,
    )
    .await
    {
        // I learned the hard way that, if you ignore the connection result, the rust compiler
        // is totally happy to drop it for you. That leads to very hard to debug errors!
        Ok((client, connection)) => {
            // Setup an async task to check for any errors on the connection (e.g. db disconnects)
            tokio::spawn(async move {
                if let Err(e) = connection.await {
                    eprintln!("Connection error: {:?}", e);
                }
            });
            Arc::new(client)
        } // Don't care about the `connection` object returned
        Err(_) => panic!("Failed to connect to DB!"),
    };

    // See [shared state via closure captures example in axum docs](https://docs.rs/axum/latest/axum/#modules)
    let app = Router::new()
        .route("/", get(|| async { "Hello, World!" }))
        .route(
            "/cats/:name",
            get(move |Path(name)| get_cat(client.clone(), name)),
        );

    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

/// Attempt to retrieve a cat from the db by name specified in a path param.
async fn get_cat(client: Arc<Client>, name: String) -> String {
    println!("Received request to GET {name}");
    let rows = match client
        .query("SELECT * FROM cats WHERE name = $1", &[&name])
        .await
    {
        Ok(rows) => rows,
        Err(e) => {
            eprintln!("Failed to retrieve cat: {e:?}");
            vec![]
        }
    };

    if rows.is_empty() {
        String::from("no cat found.")
    } else {
        format!("{:?}", Cat::from_flattened_row(rows.first().unwrap()))
    }
}
