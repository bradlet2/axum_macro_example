# axum_macro_example

Copyright © 2023 Bradley Thompson

Sets up a simple CRUD... err, R API using Axum. A small service with the purpose
of exemplifying the usage of [postgres-flatten](https://gitlab.cecs.pdx.edu/bradlet2/postgres-flatten)

## License

This program is licensed under the "MIT License". Please see the file LICENSE in the source
distribution of this software for license terms.